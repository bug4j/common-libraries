package vars

def call(String commit, String tag) {
    sh "git tag -d ${tag} || true"
    sh "git tag ${tag} ${commit}"
    sh "git push --tags"
}
