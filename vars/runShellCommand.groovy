package vars

def call(String command){
    return sh(
            script: command,
            returnStdout: true
    ).trim()
}
