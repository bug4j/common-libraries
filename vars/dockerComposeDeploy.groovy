package vars

def call(String targetStand,
         String service_name,
         String branch_name) {

    if (targetStand == 'prod' || targetStand == 'prod2' || targetStand == 'prod_usa') {
        sh "docker-compose -f docker-compose.yml stop ${service_name}"
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.prod.yml up -d --force-recreate --remove-orphans --no-build ${service_name}"
    } else if (targetStand == 'uat') {
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.uat.yml up -d --force-recreate --remove-orphans --no-build ${service_name}"
    } else if (targetStand == 'office_host') {
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.office.yml up -d --force-recreate --remove-orphans --no-build ${service_name}"
    } else if (targetStand == 'prod_china') {
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.china-prod.yml up -d --force-recreate --no-build ${service_name}"
    } else if (targetStand == 'uat_china') {
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.china-uat.yml  up -d --force-recreate --no-build ${service_name}-china-uat"
    } else {
        sh "BRANCH_NAME=${branch_name} COMPOSE_HTTP_TIMEOUT=300 docker-compose -f docker-compose.yml up -d --force-recreate --remove-orphans --no-build ${service_name}"
    }
}