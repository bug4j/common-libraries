#!/usr/bin/env groovy

def call(String buildResult, String channel) {

    slackMessage = "Сборка <${env.BUILD_URL}|${JOB_NAME} (${BUILD_NUMBER})> закончилась со статусом: ${buildResult}"

    if ( buildResult == "SUCCESS" ) {
        slackSend color: "good", channel: "${channel}", message: ":clap:${slackMessage}"
    }
    else if( buildResult == "FAILURE" ) {
        slackSend color: "danger", channel: "${channel}", message: ":scream_cat:${slackMessage}"
    }
    else if( buildResult == "UNSTABLE" ) {
        slackSend color: "warning", channel: "${channel}", message: ":astonished:${slackMessage}"
    }
    else {
        slackSend color: "danger", channel: "${channel}", message: ":face_with_raised_eyebrow:${slackMessage}"
    }
}
