#!/usr/bin/env groovy
package vars

def call(String projectId, String imageTag, String targetStand, String credentialsId){

    values_file_name = "values-staging.yaml"
    if (targetStand == "production"){
        values_file_name = "values-prod.yaml"
    }
    stage("Update Argo Config"){
        dir(projectId) {
            withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'PASSWORD', usernameVariable: 'USER')]) {
                sh "git clone https://$USER:$PASSWORD@bitbucket.org/bug4j/vsble-apps.git"
                dir("vsble-apps") {
                    sh "yq write -i vsble/$projectId/values-staging.yaml " + projectId + ".image.tag --style=double $imageTag"
                    sh "cat vsble/$projectId/values-staging.yaml"
                    sh "git config --global user.email 'argocd+vzuev@maground.com'"
                    sh "git config --global user.name 'argocd-integration'"
                    sh "git remote set-url origin https://$USER:$PASSWORD@bitbucket.org/bug4j/vsble-apps.git"
                    sh "git add vsble/$projectId/values-staging.yaml"
                    sh "git commit -am \"Updated $projectId image version with build number - $imageTag\" || true"
                    sh "git push"
                }
            }
            deleteDir()
        }
    }
}