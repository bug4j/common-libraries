package vars

import groovy.json.JsonSlurper

def imageName = "tomcat"

def encodedCred = getEncodedCredentials("docker_registry_all_perm")

def token = getDockerRegistryToken(encodedCred, imageName)

return getImageTags(token, imageName).sort().reverse()


def getImageTags(token, imageName) {
    def connection = new URL("https://docker.maground.com/v2/" + imageName + "/tags/list")
            .openConnection() as HttpURLConnection

    connection.setRequestMethod("GET")

    connection.setRequestProperty('Authorization', 'bearer ' + token)

    def slurper = new JsonSlurper()
    return slurper.parseText(connection.inputStream.text).getAt("tags")
}

def getEncodedCredentials(cred_id) {
    def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
            com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials.class,
            jenkins.model.Jenkins.instance )
    def c = creds.findResult { it.id == cred_id ? it : null }

    if (c) {
        def pair = c.username + ":" + c.password
        return pair.bytes.encodeBase64().toString()
    } else {
        throw new Exception("could not find credential for ${cred_id}")
    }
}

def getDockerRegistryToken(encodedCred, imageName) {

    def connection = new URL("https://docker.maground.com/auth?service=Docker%20registry&scope=repository%3A" + imageName + "%3Apull")
            .openConnection() as HttpURLConnection

    connection.setRequestProperty('Authorization', 'Basic ' + encodedCred)

    responceText = connection.inputStream.text

    if (connection.responseCode != 200) {
        throw new Exception("Ошибка авторизации")
    }

    def slurper = new JsonSlurper()
    def jsonResp = slurper.parseText(responceText)

    return jsonResp.getAt('token')
}
