#!/usr/bin/env groovy
package vars

def call(String buildResult, String message, String channel) {

    slackMessage = "Сборка <${env.BUILD_URL}|${JOB_NAME} (${BUILD_NUMBER})> прислала сообщение:\n${message}"

    if ( buildResult == "SUCCESS" ) {
        slackSend color: "good", channel: "${channel}", message: ":clap:${slackMessage}"
    }
    else if( buildResult == "FAILURE" ) {
        slackSend color: "danger", channel: "${channel}", message: ":scream_cat:${slackMessage}"
    }
    else if( buildResult == "UNSTABLE" ) {
        slackSend color: "warning", channel: "${channel}", message: ":astonished:${slackMessage}"
    } else {
        slackSend color: "good", channel: "${channel}", message: "${slackMessage}"
    }
}
