#!/usr/bin/env groovy
package vars

def call(String commitHash, String tag, String credentialsId){
    withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD')]){

        sh 'git config --local credential.helper \"cache --timeout=10\"'
        sh 'git config --local credential.helper \"!f() { echo username=\\$GIT_USERNAME; echo password=\\$GIT_PASSWORD; }; f"'

        try {

            try {
                sh "git tag -d ${tag} || true"
                sh "git push --delete ${tag}"
            } catch (e) {
                echo "Произошла ошибка при удалении тега в origin: " + e
            }

            if (commitHash == null || commitHash.isEmpty()) {
                sh "git tag ${tag}"
            } else {
                sh "git tag ${tag} ${commitHash}"
            }
            sh "git push --tags"

        }  catch (e) {
            echo "Произошла ошибка при отправке тега в origin: " + e
        } finally {
            echo "Подчищаем credential.helper"
            sh 'git config --local --unset credential.helper'
        }
    }
}