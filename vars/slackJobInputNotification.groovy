#!/usr/bin/env groovy
package vars

def call(String channel) {

    slackSend color: "good", channel: "${channel}", message: ":gear:Деплой на стенд ${targetStand} почти завершен.\nПодтвердите успешность установки: <${env.BUILD_URL}/input/|${env.JOB_NAME} (${BUILD_NUMBER})>"

}
