#!/usr/bin/env groovy
package vars

def call(channel = "jenkins", service = "") {

    if (service == ""){
        slackSend color: "good", channel: "${channel}", message: ":gear:Деплой на стенд ${targetStand} завершен.\nУкажите прод стенды для дальнейшей установки: <${env.BUILD_URL}/input/|${env.JOB_NAME} (${BUILD_NUMBER})>"
    } else {
        slackSend color: "good", channel: "${channel}", message: ":gear:Деплой сервиса ${service} на стенд ${targetStand} завершен.\nУкажите прод стенды для дальнейшей установки: <${env.BUILD_URL}/input/|${env.JOB_NAME} (${BUILD_NUMBER})>"
    }

}
