

def tagGitCommit(String commitHash, String tag, String credentialsId){
//    TODO к образу добавлять лейбл = хеш коммита
    withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD')]){



        //sh 'git config --global credential.helper store --file .my-credentials'
        sh 'git config --local credential.helper \"cache --timeout=10\"'
        sh 'git config --local credential.helper \"!f() { echo username=\\$GIT_USERNAME; echo password=\\$GIT_PASSWORD; }; f"'

        try {
            sh "git tag -d ${tag} || true"
            sh "git push --delete ${tag}"
        } catch (e) {
            echo "Произошла ошибка при удалении тега в origin: " + e
        }

        sh "git tag ${tag}"
        sh "git push --tags"

        sh 'git config --local --unset credential.helper'
    }
}